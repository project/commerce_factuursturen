<?php

namespace Drupal\commerce_factuursturen\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\Order;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_factuursturen\FactuurSturenServiceInterface;

/**
 * Class for the SendFactuursturen form.
 */
class SendFactuursturen extends FormBase {

  /**
   * The factuursturen service.
   *
   * @var \Drupal\commerce_factuursturen\FactuurSturenServiceInterface
   */
  protected $factuursturen;

  /**
   * Class constructor.
   */
  public function __construct(FactuurSturenServiceInterface $factuursturen) {
    $this->factuursturen = $factuursturen;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_factuursturen.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'send_factuursturen';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Order $commerce_order = NULL) {
    $form_state->set('order', $commerce_order);

    $form['merge_orders'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Merge orders ?'),
      '#description' => $this->t('Merge multiple orders for the same customer in a single invoice'),
      '#default_value' => TRUE,
    ];
    $form['order_details'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include order details ?'),
      '#description' => $this->t('Add an invoice line for each order line.'),
      '#default_value' => FALSE,
    ];

    $form['details'] = [
      '#markup' => $this->t(
        "Are you sure you want to invoice order @order_number ?",
        [
          '@order_number' => $commerce_order->getOrderNumber(),
        ]
      ),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->get('order')->get('factuursturen_id')->value) {
      $form_state->setErrorByName(
        'submit',
        $this->t("Order @order_number is already invoiced!",
          [
            '@order_number' => $form_state->get('order')->getOrderNumber(),
          ]
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order = $form_state->get('order');

    $state_item = $order->get('state')->first();
    foreach ($state_item->getTransitions() as $transition_id => $transition) {
      if ($transition_id == 'validate') {
        $state_item->applyTransition($transition);
      }
    }
    $order->save();

    $configuration = [
      'merge_orders' => $form_state->getValue('merge_orders'),
      'order_details' => $form_state->getValue('order_details'),
    ];

    $this->factuursturen->sendOrder($order, $configuration);
  }

}
