<?php

namespace Drupal\commerce_factuursturen;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Interface for the FactuurSturenService service.
 */
interface FactuurSturenServiceInterface {

  /**
   * Send the order to factuursturen.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   * @param array $configuration
   *   The sendorder options.
   */
  public function sendOrder(OrderInterface $order, array $configuration = []);

}
