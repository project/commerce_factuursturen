<?php

namespace Drupal\commerce_factuursturen;

use Drupal\commerce_store\Entity\StoreInterface;

/**
 * Interface for the FactuurSturenConnection service.
 */
interface FactuurSturenConnectionInterface {

  /**
   * Get authentication details for store.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The store.
   *
   * @return array
   *   Array with keys username and api_key.
   */
  public function getAuth(StoreInterface $store);

  /**
   * Test that there is a valid FactuurSturen auth for store.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The store.
   *
   * @return bool
   *   Boolean indicating valid authentication information.
   */
  public function isAuthValid(StoreInterface $store);

  /**
   * Get saved invoices for contact_id.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The store.
   * @param int $contact_id
   *   The contact id.
   *
   * @return array
   *   Array with saved invoices data.
   */
  public function getSavedInvoices(StoreInterface $store, $contact_id);

  /**
   * Get Factuursturen country id for given ISO country code.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The store.
   * @param string $country_code
   *   The country code.
   *
   * @return int
   *   Factuursturen country id.
   */
  public function getCountryByCode(StoreInterface $store, $country_code);

  /**
   * Send contact data to Factuursturen.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The store.
   * @param array $contact_data
   *   The contact data.
   *
   * @return int
   *   Factuursturen contact id.
   */
  public function sendContact(StoreInterface $store, array $contact_data);

  /**
   * Send invoice data to Factuursturen.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The store.
   * @param array $invoice_data
   *   The invoice data.
   *
   * @return int
   *   Factuursturen invoice id.
   */
  public function sendInvoice(StoreInterface $store, array $invoice_data);

}
