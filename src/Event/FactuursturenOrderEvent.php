<?php

namespace Drupal\commerce_factuursturen\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Event that is fired when Order data is composed for factuursturen.
 */
class FactuursturenOrderEvent extends Event {

  const ORDER_LINES_EVENT = 'factuursturen_order_lines';
  const ORDER_TITLE_EVENT = 'factuursturen_order_title';
  const ORDER_INVOICE_EVENT = 'factuursturen_order_invoice';

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  public $order;

  /**
   * The order data.
   *
   * @var array
   */
  public $orderData;

  /**
   * Constructs the object.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $order_data
   *   The order (lines) data for factuursturen.
   */
  public function __construct(OrderInterface $order, array &$order_data) {
    $this->order = $order;
    $this->orderData = &$order_data;
  }

}
