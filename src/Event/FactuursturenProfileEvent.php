<?php

namespace Drupal\commerce_factuursturen\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Event that is fired when Profile data is composed for factuursturen.
 */
class FactuursturenProfileEvent extends Event {

  const PROFILE_EVENT = 'factuursturen_profile';

  /**
   * The profile.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  public $profile;

  /**
   * The profile data.
   *
   * @var array
   */
  public $profileData;

  /**
   * Constructs the object.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param array $profile_data
   *   The profile data for factuursturen.
   */
  public function __construct(ProfileInterface $profile, array &$profile_data) {
    $this->profile = $profile;
    $this->profileData = &$profile_data;
  }

}
