<?php

namespace Drupal\commerce_factuursturen;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

use Drupal\commerce_factuursturen\Event\FactuursturenOrderEvent;
use Drupal\commerce_factuursturen\Event\FactuursturenProfileEvent;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Class for the FactuurSturen service.
 */
class FactuurSturenService implements FactuurSturenServiceInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\commerce_factuursturen\FactuurSturenConnectionInterface
   */
  protected $factuurSturenConnection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new FactuurSturenService object.
   */
  public function __construct(
    FactuurSturenConnectionInterface $factuur_sturen_connection,
    EntityTypeManagerInterface $entity_type_manager,
    EventDispatcherInterface $event_dispatcher,
    MessengerInterface $messenger) {
    $this->factuurSturenConnection = $factuur_sturen_connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->messenger = $messenger;
  }

  /**
   * Create a contact for the $customer_profile.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The store.
   * @param \Drupal\profile\Entity\ProfileInterface $customer_profile
   *   The profile info to create a contact for.
   *
   * @return int
   *   The factuursturen contact id.
   */
  public function sendProfile(StoreInterface $store, ProfileInterface $customer_profile) {
    $factuursturen_ids = $customer_profile->getData('factuursturen_ids');
    if (empty($factuursturen_ids)) {
      $factuursturen_ids = [];
    }

    $auth = $this->factuurSturenConnection->getAuth($store);

    if (!empty($factuursturen_ids[$auth['username']])) {
      return $factuursturen_ids[$auth['username']];
    }

    $address = $customer_profile->get('address');
    $profile_data = [
      'contact' => "{$address->given_name} {$address->family_name}",
      'showcontact' => TRUE,
      'company' => $address->organization,
      'address' => $address->address_line1,
      'zipcode' => $address->postal_code,
      'city' => $address->locality,
      'country' => $this->factuurSturenConnection->getCountryByCode(
        $store, $address->country_code),
      'email' => $customer_profile->getOwnerId() ? $customer_profile->getOwner()->getEmail() : '',
      'tax_shifted' => FALSE,
      'sendmethod' => 'email',
      'active' => TRUE,
      'currency' => 'EUR',
      'tax_type' => 'extax',
    ];
    if ($customer_profile->hasField('tax_number') &&
      $customer_profile->get('tax_number')->value) {
      $profile_data['taxnumber'] = $customer_profile->get('tax_number')->value;
    }

    $event = new FactuursturenProfileEvent($customer_profile, $profile_data);
    $this->eventDispatcher->dispatch(FactuursturenProfileEvent::PROFILE_EVENT, $event);

    $factuursturen_id = $this->factuurSturenConnection->sendContact($store, $profile_data);

    $factuursturen_ids[$auth['username']] = $factuursturen_id;
    $customer_profile->setData('factuursturen_ids', $factuursturen_ids);
    $customer_profile->save();

    return $factuursturen_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItemData($order_item) {
    $taxrate = 0;
    $tax_included = FALSE;
    foreach ($order_item->getAdjustments(['tax']) as $tax) {
      $taxrate = bcmul($tax->getPercentage(), '100', 0);
      $tax_included = $tax->isIncluded();
    }

    $price = $order_item->getUnitPrice();
    if ($tax_included) {
      $price = $price->divide(
        bcdiv(
          bcadd(
            '100',
            $taxrate,
            2),
          '100',
          2)
      );
    }

    $line_data = [
      'amount' => $order_item->getQuantity(),
      'amount_desc' => '',
      'description' => $order_item->getTitle(),
      'tax_rate' => $taxrate,
      'price' => $price->getNumber(),
    ];
    return $line_data;
  }

  /**
   * {@inheritdoc}
   */
  public function profileForOrder(OrderInterface $order) {
    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = $order->getBillingProfile();
    if (empty($billing_profile)) {
      $profiles = $this->entityTypeManager
        ->getStorage('profile')
        ->loadByProperties([
          'type' => 'customer',
          'uid' => $order->getCustomerId(),
          'is_default' => TRUE,
        ]);
      if (count($profiles)) {
        /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
        $billing_profile = reset($profiles);
      }
    }

    while ($billing_profile &&
      !empty($billing_profile->getData('address_book_profile_id'))) {
      $billing_profile = Profile::load(
        $billing_profile->getData('address_book_profile_id'));
    }

    return $billing_profile;
  }

  /**
   * Get invoice data for Factuursturen contact.
   *
   * Return data includes previous invoice for contact for merging
   * if requested.
   *
   * @returns array
   *   An invoice data array ready to send to factuursturen.
   */
  public function invoiceForOrder($store, OrderInterface $order, array $configuration = []) {
    $billing_profile = $this->profileForOrder($order);
    if (!$billing_profile) {
      $this->messenger->addError(
        $this->t("Could not send order ':order_number': No valid profile.", [
          ':order_number' => $order->getOrderNumber(),
        ]
        )
      );
      return;
    }

    $contact_id = $this->sendProfile(
      $store,
      $billing_profile
    );

    $invoice_data = [];
    if ($configuration['merge_orders'] ?? FALSE) {
      $invoice_data = $this->factuurSturenConnection->getSavedInvoices(
        $store, $contact_id);
    }

    if (empty($invoice_data)) {
      // New invoice.
      $new_invoice_data = [
        'clientnr' => $contact_id,
        'lines' => [],
        'savename' => "{$order->id()}-{$order->getOrderNumber()}",
        'action' => 'save',
        'alreadypaid' => '0',
      ];
    }
    else {
      // Merge invoice.
      $new_invoice_data = [
        'clientnr' => $invoice_data['clientnr'],
        'lines' => $invoice_data['lines'],
        'savename' => $invoice_data['name'],
        'overwrite_if_exist' => TRUE,
        'action' => 'save',
        'alreadypaid' => '0',
      ];

      if ($invoice_data['alreadypaid']) {
        $invoice_data['alreadypaid'] = $invoice_data['alreadypaid'];
      }
    }
    return $new_invoice_data;
  }

  /**
   * {@inheritdoc}
   */
  public function sendOrder(OrderInterface $order, array $configuration = []) {
    if ($order->get('factuursturen_id')->value) {
      return $order->get('factuursturen_id')->value;
    }

    $store = $order->getStore();

    if (!$this->factuurSturenConnection->isAuthValid($store)) {
      $this->messenger->addError(
        $this->t("Could not send order. Configure your factuursturen account.")
      );
      return;
    }

    $invoice_data = $this->invoiceForOrder(
      $store, $order, $configuration);

    if (!$invoice_data) {
      return;
    }

    if ($order->getTotalPaid()->getNumber() > 0) {
      $invoice_data['alreadypaid'] = bcadd(
        $invoice_data['alreadypaid'],
        $order->getTotalPaid()->getNumber(),
        2
      );
    }

    if ($configuration['order_details'] ?? FALSE) {
      $lines_for_order = $this->groupedItemLinesForOrder($order);
    }
    else {
      $lines_for_order = $this->summaryInvoiceLinesForOrder($order);
    }

    $event = new FactuursturenOrderEvent($order, $lines_for_order);
    $this->eventDispatcher->dispatch(FactuursturenOrderEvent::ORDER_LINES_EVENT, $event);

    $invoice_data['lines'] = array_merge(
      $invoice_data['lines'],
      $lines_for_order
    );

    $event = new FactuursturenOrderEvent($order, $invoice_data);
    $this->eventDispatcher->dispatch(FactuursturenOrderEvent::ORDER_INVOICE_EVENT, $event);

    $factuursturen_id = $this->factuurSturenConnection->sendInvoice(
      $store, $invoice_data);

    $order->set('factuursturen_id', $factuursturen_id);
    $order->save();

    return $factuursturen_id;
  }

  /**
   * Get invoice title for order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   *
   * @return string
   *   The text to display on the invoice.
   */
  public function titleLineForOrder(OrderInterface $order) {
    $complete = date('Y-m-d', $order->getCompletedTime());
    $data = [
      'title' => "{$order->getOrderNumber()} ({$complete})",
    ];
    $event = new FactuursturenOrderEvent($order, $data);
    $this->eventDispatcher->dispatch(FactuursturenOrderEvent::ORDER_TITLE_EVENT, $event);

    return $data['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function itemLinesForOrder(OrderInterface $order) {
    $item_lines = [];
    foreach ($order->getItems() as $order_item) {
      $item_lines[] = $this->getOrderItemData($order_item);
    }
    return $item_lines;
  }

  /**
   * Get grouped item lines for order.
   *
   * Includes a title line for the order and a line for each order item.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   *
   * @return array
   *   The text to display on the invoice.
   */
  public function groupedItemLinesForOrder(OrderInterface $order) {
    $lines_for_order = array_merge(
      [
        [
          'description' => $this->titleLineForOrder($order),
        ],
      ],
      $this->itemLinesForOrder($order)
    );
    return $lines_for_order;
  }

  /**
   * Get summary invoice lines for order.
   *
   * Returns one item line per tax rate for the order, with
   * the total base amount for this tax rate.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   *
   * @return array
   *   The summary invoice lines.
   */
  public function summaryInvoiceLinesForOrder(OrderInterface $order) {
    $grouped = [];

    // Group items by taxrate.
    foreach ($order->getItems() as $order_item) {
      $data = $this->getOrderItemData($order_item);
      if (!array_key_exists($data['tax_rate'], $grouped)) {
        $grouped[$data['tax_rate']] = [
          'amount' => '0',
        ];
      }
      $grouped[$data['tax_rate']]['amount'] = bcadd(
        $grouped[$data['tax_rate']]['amount'],
        bcmul(
          $data['price'],
          $data['amount'],
          2
        ),
        2
      );
    }

    // Line for each taxrate.
    $lines = [];
    foreach ($grouped as $tax_rate => $data) {
      $lines[] = [
        'amount' => 1,
        'amount_desc' => '',
        'description' => $this->titleLineForOrder($order),
        'tax_rate' => $tax_rate,
        'price' => $data['amount'],
      ];
    }

    return $lines;
  }

}
