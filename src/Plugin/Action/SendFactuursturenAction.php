<?php

namespace Drupal\commerce_factuursturen\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_factuursturen\FactuurSturenServiceInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Send order to factuursturen.
 *
 * @Action(
 *   id = "send_factuursturen_action",
 *   label = @Translation("Send Factuursturen"),
 *   type = "commerce_order"
 * )
 */
class SendFactuursturenAction extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface, PluginFormInterface {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Factuursturen service.
   *
   * @var \Drupal\commerce_factuursturen\FactuurSturenServiceInterface
   */
  protected $factuurSturen;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MessengerInterface $messenger, FactuurSturenServiceInterface $factuur_sturen) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->messenger = $messenger;
    $this->factuurSturen = $factuur_sturen;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('messenger'),
      $container->get('commerce_factuursturen.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merge_orders' => '',
      'order_details' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['merge_orders'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Merge orders ?'),
      '#description' => $this->t('Merge multiple orders for the same customer in a single invoice'),
      '#default_value' => TRUE,
    ];
    $form['order_details'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include order details ?'),
      '#description' => $this->t('Add an invoice line for each order line.'),
      '#default_value' => FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
    $this->factuurSturen->sendOrder($entity, $this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getState()->value != 'completed') {
      $result = AccessResult::forbidden();
    }
    else {
      $result = $object->access('update', $account, TRUE);
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

}
