<?php

namespace Drupal\commerce_factuursturen;

use Drupal\Core\Config\ConfigFactoryInterface;

use Drupal\commerce_store\Entity\StoreInterface;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;

use GuzzleHttp\ClientInterface;

/**
 * The FactuurSturen API Connection service.
 */
class FactuurSturenConnection implements FactuurSturenConnectionInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a FactuursturenConnection object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $country_repository
   *   The country repository.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CountryRepositoryInterface $country_repository,
    ClientInterface $http_client) {
    $this->configFactory = $config_factory;
    $this->countryRepository = $country_repository;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  protected function getGlobalAuth() {
    $config = $this->configFactory->get('commerce_factuursturen.settings');
    return [
      'username' => $config->get('username'),
      'api_key' => $config->get('api_key'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getAuthForStore(StoreInterface $store) {
    return [
      'username' => $store->get('factuursturen_api_user')->value,
      'api_key' => $store->get('factuursturen_api_key')->value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAuth(StoreInterface $store) {
    $auth = $this->getAuthForStore($store);
    if (empty($auth['username'])) {
      $auth = $this->getGlobalAuth();
    }
    return $auth;
  }

  /**
   * {@inheritdoc}
   */
  public function isAuthValid(StoreInterface $store) {
    return !empty($this->getAuth($store)['username']);
  }

  /**
   * {@inheritdoc}
   */
  protected function request(StoreInterface $store, $method, $path, $data = []) {
    $auth = $this->getAuth($store);

    $data['headers']['Accept'] = 'application/json';
    $data['auth'] = [
      $auth['username'],
      $auth['api_key'],
    ];
    $response = $this->httpClient->request(
      $method,
      "https://www.factuursturen.nl/api/v1/{$path}",
      $data
    );
    if ($method === 'POST') {
      // Factuursturen doesn't reply with json on POST.
      return $response->getBody()->getContents();
    }
    return json_decode($response->getBody(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getSavedInvoices(StoreInterface $store, $contact_id) {
    $data = $this->request($store, "GET", "invoices_saved");
    $invoices = array_filter(
      $data,
      function ($item) use ($contact_id) {
        return $item['clientnr'] == $contact_id;
      }
    );
    if (count($invoices)) {
      return reset($invoices);
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCountryByCode(StoreInterface $store, $country_code) {
    $english_name = $this->countryRepository
      ->get($country_code, 'en')->getName();

    $info = $this->request($store, "GET", "countrylist/en");
    $country_info = array_filter(
      $info,
      function ($c_info) use ($english_name) {
        return $c_info['name'] === $english_name;
      }
    );
    if (count($country_info) == 1) {
      return reset($country_info)['id'];
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function sendContact(StoreInterface $store, array $contact_data) {
    return $this->request($store, 'POST', 'clients', ['json' => $contact_data]);
  }

  /**
   * {@inheritdoc}
   */
  public function sendInvoice(StoreInterface $store, array $invoice_data) {
    return $this->request($store, 'POST', 'invoices', ['json' => $invoice_data]);
  }

}
