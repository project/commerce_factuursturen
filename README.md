CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration


INTRODUCTION
------------

The Commerce Factuursturen module integrates your drupal commerce with
the [Factuursturen.nl](https://www.factuursturen.nl) service. 

It allows you to create invoices for you orders in the factuursturen
application.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_factuursturen

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_factuursturen


REQUIREMENTS
------------

This module requires the following modules:

 * [Commerce](https://www.drupal.org/project/commerce)


RECOMMENDED MODULES
-------------------

 * [Views Bulk Operations](https://www.drupal.org/project/views_bulk_operations):
   When enabled, allow you to send multiple orders at once.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------
 
 * Configure your factuursturen.nl account in Commerce » Configuration » Stores:

   - Edit your store and fill out the factuursturen.nl username and api key.
